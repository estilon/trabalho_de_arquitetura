app.controller('questaoController',['$scope','$http','$cookies',function($scope,$http,$cookies){

    //TODO lembrar de setar esses valores quando for listas as questões

    $scope.grupo = "56aae13de6ac168563d69c73";
    $scope.autor = $cookies.get("pessoa");
    $scope.alternativas = [];

    $scope.questoesDeUmGrupo = function(idDoGrupo){

        $scope.grupo = idDoGrupo;

        console.log(idDoGrupo);

        $http.get('http://localhost:3000/todasAsQuestoesDeUmGrupo?grupo='+idDoGrupo).
        then(function(response){
            $scope.questoes = response.data;
            console.log($scope.questoes);
        },function(response){
            console.log(response);
        });
    };

    $scope.sendToServer = function(){
        var questao = {
            texto : $scope.texto,
            resposta : $scope.resposta,
            alternativas : $scope.alternativas,
            grupo : $scope.grupo,
            autor : $scope.autor

        };

        console.log(questao);

        $http.post('http://localhost:3000/questao',questao,null).then(function(response){
            console.log(response);
        },function(response){
            console.log(response);
        });
    };
}]);
