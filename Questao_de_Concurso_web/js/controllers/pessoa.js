app.controller('pessoaController',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){

    $scope.login = function(){
        var pessoa = {email : $scope.email, senha : $scope.senha};
        console.log(pessoa);
        $http.post('http://localhost:3000/login',pessoa,null).then(function(response){
            if(response.data.status == "sucesso") {
                $cookies.put("pessoa", response.data.id);
                $window.location.href="index.html";
                console.log(response);
            }
        },function(response){
            console.log(response);
        });
    };
}]);