app.controller('myCtrl', ['$scope', function($scope) {

    $scope.show_card_grupo = true;
    $scope.show_card_questao = false;
    $scope.show_formularo_grupo = false;
    $scope.show_formulario_questoes = false;
    $scope.titulo_pagina= "Grupos";
    $scope.na_lista_grupos = true;
    
   
   
    
    $scope.mostrar_grupos = function(){
        $scope.show_card_grupo = true;
        $scope.show_card_questao = false;
        $scope.show_formularo_grupo = false;
        $scope.show_formulario_questoes = false;
        $scope.titulo_pagina = "Grupos";
        $scope.na_lista_grupos = true;
       
    };
    $scope.mostrar_questoes = function(){
        $scope.show_card_grupo = false;
        $scope.show_card_questao = true;
        $scope.show_formularo_grupo = false;
        $scope.show_formulario_questoes = false;
        $scope.titulo_pagina = "Questões";
        $scope.na_lista_grupos = false;
    };
    $scope.mostrar_formulario_grupo = function(){
        $scope.show_card_grupo = false;
        $scope.show_card_questao = false;
        $scope.show_formularo_grupo = true;
        $scope.show_formulario_questoes = false;
        $scope.titulo_pagina = "Novo grupo";
        $scope.na_lista_grupos = true;
    };
    $scope.mostrar_formulario_questao = function(){
        $scope.show_card_grupo = false;
        $scope.show_card_questao = false;
        $scope.show_formularo_grupo = false;
        $scope.show_formulario_questoes = true;
        $scope.titulo_pagina = "Nova questão";
        $scope.na_lista_grupos = false;
    };
    
    $scope.mostrar_formulario = function(){
        if($scope.na_lista_grupos){
            $scope.mostrar_formulario_grupo();
        }else{
            $scope.mostrar_formulario_questao();
        };
        
    }
    
    $scope.mostrar_card= function(){
        if(!$scope.na_lista_grupos){
            $scope.mostrar_questoes();
        }else{
            $scope.mostrar_grupos();
        };
    };

    $scope.varificar_aba_ativa = function(event){
        
            console.log(event.target);
            document.getElementById("Todos").className="waves-effect waves-dark";
            document.getElementById("Favoritos").className="waves-effect waves-dark";
            document.getElementById("Novo").className="waves-effect waves-dark";
            document.getElementById(event.target.innerHTML).className="waves-effect waves-dark active";
            
       
    }; 

}]);