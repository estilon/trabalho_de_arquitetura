app.controller('grupoController',['$scope','$http','$cookies',function($scope,$http,$cookies){
    $scope.grupos = [];
    $scope.sendToServer = function(idForm){
        var grupo = {nome : $scope.nome, descricao : $scope.descricao};
        console.log(grupo);
        $http.post('/users/grupo',grupo,null).then(function(response){
            console.log(response);
            if(response.data.status == 'sucesso'){
                $scope.limparFormulario(idForm);
                $scope.todosOsGrupos();
                alert("Novo grupo cadastrado com sucesso");
            }else{
                alert("O grupo não pode ser cadastrado");
            }
        },function(response){
            console.log(response);

        });




    };

    $scope.participar = function(idDoGrupo){
        var pessoa = $cookies.get("pessoa");

        $http.post('/users/adicionarMembro',
            {idDoGrupo : idDoGrupo, idDoMembro : pessoa},{cache : false}).then(
            function(response){
                $scope.todosOsGrupos();
                if(response.data.status = 'sucesso'){
                    socket.emit('join', {idDoGrupo : idDoGrupo});
                }
                console.log("response de adicionar");
                console.log(response);
            },function(response){
                consoel.log(response);
            });
    }


    $scope.todosOsGrupos = function(){

        $http.get('/users/todosOsGrupos').then(
            function(response){
                var grupos = response.data;
                grupos.forEach(function(elem){
                    elem.ismembro = false;
                    elem.membros.forEach(function(membro){
                        if(membro == $cookies.get("pessoa")) {
                            elem.ismembro = true;
                            socket.emit('join', {idDoGrupo: elem._id});
                        }
                        console.log(elem);
                    })

                });

                $scope.grupos = grupos;
            },
            function(response){
                console.log(response);
            });
    }

    $scope.pesquisar = function($event){
        $http.get('/users/pesquisar').then(
            function(response){
                var grupos = response.data;
                grupos.forEach(function(elem){
                    elem.ismembro = false;
                    elem.membros.forEach(function(membro){
                        if(membro == $cookies.get("pessoa")) {
                            elem.ismembro = true;
                        }
                        console.log(elem);
                    })

                });

                $scope.grupos = grupos;
            },
            function(response){
                console.log(response);
            });

    };

    $scope.membro = function(grupo){
        grupo.membros.forEach(function(membro){
            if(membro == $cookies.get("pessoa")) {
                console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                return true;
            }
        });

        return false;
    };

    $scope.limparFormulario = function(idForm){

        console.log("passou aqui");
        var form = document.getElementById(idForm);
        var inputs = form.querySelectorAll('input');
        var textarea = form.querySelectorAll('textarea');
        for (var i = 0; i < inputs.length; i++) {

            inputs[i].value = '';
            textarea[i].value = '';

        }
    };



}]);