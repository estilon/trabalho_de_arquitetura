app.controller('pessoaController',['$scope','$http','$cookies','$window',function($scope,$http,$cookies,$window){

    $scope.login = function(){
        var pessoa = {email : $scope.email, senha : $scope.senha};
        console.log(pessoa);
        $http.post('/login',pessoa,null).then(function(response){
            if(response.data.status == "sucesso") {
                $cookies.put("pessoa", response.data.id);
                $window.location.href="/";
                console.log(response);
            }
        },function(response){
            console.log(response);
        });
    };

    $scope.cadastrar = function(){

        var pessoa = {
            nome : $scope.nome,
            email : $scope.email,
            senha : $scope.senha,
            confimacao : $scope.confirmacao
        };

        $http.post("/pessoa",pessoa,null).then(
            function(response){
                if(response.data.status == "sucesso") {



                }
                $cookies.put("pessoa", response.data.id);
                $window.location.href="/";
                console.log(response);
            },
            function(response){
                console.log(response);
            });
    };

    $scope.logout = function(){
        $http.get('/logout').then(function(response){
            if(response.data.status == "sucesso") {
                $window.location.href="/";
                console.log(response);
            }
        },function(response){
            console.log(response);
        })
    };

    $scope.seRegistarNosSockets = function(){
        var pessoa = $cookies("pessoa");

        $http.get("/users/todosOsGruposDeUmaPessoa?idDaPessoa="+pessoa).then(
            function(response){
                console.log(response);
            },function(response){
                console.log(response);
            }
        );
    }
}]);