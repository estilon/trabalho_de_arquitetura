app.controller('questaoController',['$scope','$http','$cookies',function($scope,$http,$cookies){

    //TODO lembrar de setar esses valores quando for listas as questões

    //$scope.grupo = "56aae13de6ac168563d69c73";
    $scope.autor = $cookies.get("pessoa");
    $scope.alternativas = [];
    $scope.questoes = [];

    $scope.questoesDeUmGrupo = function(idDoGrupo){

        $scope.grupo = idDoGrupo;

        console.log("esse grupo está sendo mostrado agora");
        $scope.grupo = idDoGrupo;
        console.log($scope.grupo);

        $http.get('/users/todasAsQuestoesDeUmGrupo?grupo='+idDoGrupo).
        then(function(response){
            $scope.questoes = response.data;
            console.log($scope.questoes);
        },function(response){
            console.log(response);
        });
    };

    $scope.sendToServer = function(idForm){
        var questao = {
            texto : $scope.texto,
            resposta : $scope.resposta,
            alternativas : $scope.alternativas,
            grupo : $scope.grupo,
            autor : $scope.autor

        };

        $http.post('/users/questao',questao,null).then(function(response){
            console.log(response);
            if(response.data.status == 'sucesso'){
                $scope.limparFormulario(idForm);
                $scope.questoesDeUmGrupo(questao.grupo);
                alert("Nova questão cadastrada com sucesso");
            }else{
                alert("A nova questão não pode ser cadastrada");
            }
        },function(response){
            console.log(response);
            
        });
        
        
        
    };
    
    $scope.limparFormulario = function(idForm){
        
        console.log("passou aqui");
        var form = document.getElementById(idForm);
        var inputs = form.querySelectorAll('input');
        var textarea = form.querySelectorAll('textarea');
        var checkbox = form.querySelectorAll('input[type=checkbox]');
        for (var i = 0; i < inputs.length; i++) {
            
                inputs[i].value = '';
                //checkbox[i].value ='';
        }
        for (var i = 0; i < textarea.length; i++) {

                textarea[i].value = '';
                //checkbox[i].value ='';
        }
        for (var i = 0; i < checkbox.length; i++) {

               checkbox[i].checked =false;
        }
    };

    $scope.corrigir = function(idresposta, iddiv){
        var resposta = document.getElementById(idresposta);
        var div = document.getElementById(iddiv);
        var checkboxes = div.querySelectorAll('input[type=checkbox]');

        console.log(resposta.innerHTML);
        for (var i = 0; i < checkboxes.length; i++) {

            if(checkboxes[i].checked ==true && i+1 == resposta.innerHTML){
                alert("Parabéns você acertou");
                break;
            }else{
                alert("Você errou");
                break;
            };
        }


    };
}]);
