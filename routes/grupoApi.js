var express = require('express');
var router = express.Router();
var Grupo = require('../persistence/models/grupo');
var constantes = require('../constantes/constantes');
var mongoose = require('mongoose');

router.get('/todosOsGrupos',function(req, res, next){

    Grupo.find({},function(err, grupos){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(grupos);
    });

});

router.get('/todosOsGruposDeUmaPessoa',function(req, res, next){
    var idDaPessoa = new mongoose.Types.ObjectId(req.query.idDaPessoa);
    Grupo.find({membros : idDaPessoa},function(err, grupos){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(grupos);
    });

});

router.get('/pesquisar',function(req, res, next){

    var q = req.query.q;
    Grupo.find({membros : idDaPessoa},function(err, grupos){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(grupos);
    });

});

router.get('/grupo', function(req, res, next){

    var idDoGrupo = new mongoose.Types.ObjectId(req.query.idDoGrupo);

    Grupo.findOne({_id : idDoGrupo},function(err, grupo){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(grupo);
    });
});

router.post('/grupo', function(req, res, next){
    var grupo = new Grupo();

    grupo.nome = req.body.nome;
    grupo.descricao = req.body.descricao;

    grupo.save(function(err){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.json({status : constantes.MSG_SUCESSO});
    });

});

router.post('/atualizarGrupo', function(req, res, next){

    var idDoGrupo = new mongoose.Types.ObjectId(req.body.idDoGrupo);

    var grupo = {
        nome : req.body.nome,
        descricao : req.body.descricao,
        membros : req.body.membros
    };

    Grupo.update({_id : idDoGrupo},{$set : grupo}, {multi : false},function(err, numeroDeAfetados){
        if(err || numeroDeAfetados.nModified == 0) {
            console.log(err);
            res.json({status: constantes.MSG_FALHA});
        }
        else
            res.json({status : constantes.MSG_SUCESSO});
    });

});

router.post('/adicionarMembro', function(req, res, next){

    var idDoGrupo = new mongoose.Types.ObjectId(req.body.idDoGrupo);
    var idDoMembro = new mongoose.Types.ObjectId(req.body.idDoMembro);

    console.log(req.body.idDoGrupo);

    Grupo.update(
        {_id : idDoGrupo, membros : {$ne : idDoMembro}},{$push : {membros : idDoMembro}},
        {multi : false},
        function(err, numeroDeAfetados){
            console.log(err);
            console.log(numeroDeAfetados.nModified);
            if(err || numeroDeAfetados.nModified == 0) {
                res.send({status: constantes.MSG_FALHA});
            }
            else {
                res.send({status: constantes.MSG_SUCESSO});
            }
    });
});

module.exports = router;