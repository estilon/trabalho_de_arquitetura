var express = require('express');
var router = express.Router();
var Questao = require('../persistence/models/questao');
var Grupo = require('../persistence/models/grupo');
var constantes = require('../constantes/constantes');
var mongoose = require('mongoose');

router.get('/todasAsQuestoesDeUmGrupo', function(req, res, next) {

    var ObjectId = mongoose.Types.ObjectId;
    var idDoGrupo = new ObjectId(req.query.grupo);

    Questao.find({grupo : idDoGrupo },function(err,questoes){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(questoes);
    });
});

router.get('/questao', function(req, res, next) {

    var ObjectId = mongoose.Types.ObjectId;
    var idDaQuestao = new ObjectId(req.query.idDaQuestao);

    Questao.findOne({_id : idDaQuestao},function(err, questao){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.send(questao);
    });
});

router.post('/questao',function(req,res,next){

    var io = req.io;

    var questao = new Questao();

    questao.texto = req.body.texto;
    questao.resposta = req.body.resposta;
    questao.alternativas = req.body.alternativas;
    questao.grupo = new mongoose.Types.ObjectId(req.body.grupo);
    questao.autor = new mongoose.Types.ObjectId(req.body.autor);

    questao.save(function(err){
        if(!err) {
            Grupo.findOne({_id:questao.grupo}, function(err, grupo){
                if(grupo != null)
                    io.sockets.in(questao.grupo).emit(constantes.NOTIFICACAO_NOVA_QUESTAO, {nomeDoGrupo : grupo.nome});
            });
            res.json({status: constantes.MSG_SUCESSO});
        }
        else
            res.json({status : constantes.MSG_FALHA});

    });

});

router.post('/atualizarQuestao',function (req,res,next){
    //TODO
    res.json({status : constantes.MSG_NAO_IMPLEMENTADO});
});

module.exports = router;