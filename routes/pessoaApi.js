var express = require('express');
var router = express.Router();
var Pessoa = require('../persistence/models/pessoa');
var constantes = require('../constantes/constantes');
var mongoose = require('../persistence/mongoose');

router.post('/pessoa', function(req, res, next){
    var pessoa = new Pessoa();

    pessoa._id = new mongoose.Types.ObjectId();
    pessoa.nome = req.body.nome;
    pessoa.email = req.body.email;
    pessoa.senha = req.body.senha;

    pessoa.save(function(err){
        if(err)
            res.json({status : constantes.MSG_FALHA});
        else
            res.json({status : constantes.MSG_SUCESSO, id : pessoa._id});
    });

});

module.exports = router;