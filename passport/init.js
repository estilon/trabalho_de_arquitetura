var facebook = require('./facebook');
var Pessoa = require('../persistence/models/pessoa');

module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(pessoa, done) {
        console.log('serializing user: ');console.log(pessoa);
        done(null, pessoa._id);
    });

    passport.deserializeUser(function(id, done) {
        Pessoa.findById(id, function(err, pessoa) {
            console.log('deserializing user:',pessoa);
            done(err, pessoa);
        });
    });

    // Setting up Passport Strategies for Facebook and Twitter
    facebook(passport);

}