/**
 * Created by estilon on 22/01/16.
 */

var mongoose = require('../mongoose');
var Schema = mongoose.Schema;

var GrupoSchema = new Schema({
    nome : {type : String},
    descricao : {type : String},
    membros : {type : [Schema.Types.ObjectId]},
});

var Grupo = mongoose.model('grupo',GrupoSchema);

module.exports = Grupo;