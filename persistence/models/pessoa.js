/**
 * Created by estilon on 22/01/16.
 */
var mongoose = require('../mongoose');
var Schema = mongoose.Schema;

var PessoaSchema = new Schema({
    nome : String,
    senha : String,
    email: String
});

var Pessoa = mongoose.model('pessoa',PessoaSchema);

module.exports = Pessoa;