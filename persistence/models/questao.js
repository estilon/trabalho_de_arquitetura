var mongoose = require('../mongoose');
var Schema = mongoose.Schema;

var QuestaoSchema = new Schema({
    texto : {type : String},
    alternativas : {type:[String]},
    resposta : {type : Number},
    autor : {type : Schema.Types.ObjectId},
    grupo : {type : Schema.Types.ObjectId},
    comentarios : [String]
});

var Questao = mongoose.model('questao',QuestaoSchema);

module.exports = Questao;

