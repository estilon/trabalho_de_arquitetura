var Pessoa = require('../models/pessoa');
var constantes = require('../../constantes/constantes');

var RepositorioDePessoa = {};

RepositorioDePessoa.getPessoaPeloId = function (id, callback){};

RepositorioDePessoa.validateToken = function(token, callback){

   Pessoa.findOne({token : token},function(err, doc){

        if(err){
            callback({status : constantes.FALHA_DE_BANCO},null);
        }else{
            if(doc != null)
                callback({status : constantes.TOKEN_VALIDO});
            else
                callback({status : constantes.TOKEN_INVALIDO},null);
        }
   });
};

module.exports = RepositorioDePessoa;
