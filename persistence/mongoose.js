var mongoose = require('mongoose');

var db_config = require('../config/db_config');

mongoose.connect(db_config.url,function(err){
    if(err)
        console.log(err);
});

module.exports = mongoose;